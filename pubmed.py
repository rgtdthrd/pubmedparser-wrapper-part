import os
import re
import hashlib
from ftplib import FTP
from typing import Iterable, List
from storage import default_cache_dir


BASE_URL = "ftp.ncbi.nlm.nih.gov"
NAME_PREFIX = "pubmed23n"  # Update yearly
NAME_REGEX_TEMPLATE = r".*{}({})\.xml\.gz$".format(NAME_PREFIX, "{}")


def _download_files(src_dir: str, args: List[str], cache_dir: str):
    file_names = [f"{NAME_PREFIX}{arg}.xml.gz" for arg in args]
    file_urls = [
        f"{BASE_URL}/{src_dir}/{file_name}" for file_name in file_names
    ]

    with FTP('ftp.ncbi.nlm.nih.gov') as ftp:
            ftp.login()
            ftp.cwd(src_dir)
            for file_url in file_urls:
                file_name = file_url.split("/")[-1]
                with open(file_name, "wb") as f:
                    ftp.retrbinary(f"RETR {file_name}", f.write)
                with open(f"{file_name}.md5", "wb") as f:
                    ftp.retrbinary(f"RETR {file_name}.md5", f.write)
    
    md5_file_names = [f"{f}.md5" for f in file_names]
    for file_name, md5_file_name in zip(file_names, md5_file_names):
        with open(md5_file_name, "rb") as f:
            expected_md5 = f.read().split()[0]
        with open(file_name, "rb") as f:
            actual_md5 = hashlib.md5(f.read()).hexdigest()
        if actual_md5 != expected_md5:
            print(f"{file_name} failed md5sum check, deleting")
            os.unlink(file_name)
        os.unlink(md5_file_name)

def _list_local_pubmed_files(path: str) -> List[str]:
    files = os.listdir(path)
    regex = re.compile(NAME_REGEX_TEMPLATE.format("[0-9]{4}"))
    return [f for f in files if regex.match(f)]


def list_files(src_dir: str) -> List[str]:
    files: List[str] = []
    with FTP(BASE_URL) as ftp:
        ftp.login()
        ftp.cwd("pubmed/" + src_dir)
        ftp.retrlines("NLST", files.append)
    return [f for f in files if f.endswith(".xml.gz")]


def _missing_files(desired_files: List[str], cache_dir: str) -> List[str]:
    local_files = sorted(_list_local_pubmed_files(cache_dir))
    intersect = sorted(list(set(desired_files) & set(local_files)))
    return sorted(list(set(desired_files) - set(intersect)))


def _filter_to_file_numbers(files: List[str], numbers: Iterable[int]):
    number_pattern = "|".join([f"{n}" for n in numbers])
    print(number_pattern)
    regex = re.compile(NAME_REGEX_TEMPLATE.format(number_pattern))
    return [f for f in files if regex.match(f)]


def download_pubmed_data(
    files: str | int | Iterable[int] = "all",
    source: str = "updatefiles",
    cache_dir: str = default_cache_dir(NAME_PREFIX),
):
    if isinstance(files, str) and files != "all":
        raise TypeError('Files is not of type int or "all".')

    if isinstance(files, int):
        files = [files]

    remote_files = list_files(source)
    if not isinstance(files, str):
        remote_files = _filter_to_file_numbers(remote_files, files)

    missing_files = _missing_files(remote_files, cache_dir)

    if not os.path.exists(cache_dir):
        os.makedirs(cache_dir)
    os.chdir(cache_dir)

    if missing_files:
        print("Downloading files...")
        _download_files(source, missing_files, cache_dir)
        print("Finished downloading files.")
