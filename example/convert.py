import os

top_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

xml_dir = 'data'
cache_dir = 'cache'
import_dir = 'import'
structure_file = 'example/structure.yml'

for directory in [cache_dir, import_dir, xml_dir]:
    if os.path.exists(directory):
        os.system(f"rm -r {directory}")

os.system(f"helpers/download_pubmed_data --destination {xml_dir} {'0001..0003'}")


os.system(f"result/bin/read_xml --structure-file={structure_file} --cache={cache_dir} {xml_dir}/*")