use command line below to compile the so file:
python3 setup.py build_txt --inplace

it will create a new folder in Wrapper called "build"
There will be (lib.linux) (src) (tmp.linux) inside

all the c file and corresponding h file will be compile to o file and stored in src
then all the o file will be built into module.o by cython and stored in tmp.linux
finally the callable Dynamic Linking Library will be created as so file and stored in lib.linux
