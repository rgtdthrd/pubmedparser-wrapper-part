from setuptools.extension import Extension
from Cython.Build import cythonize
from setuptools import find_packages, setup

import os


def find_files(root, ext):
    ret = list()
    if os.path.exists(root):
        for file in os.listdir(root):
            if file.endswith(ext):
                ret.append(os.path.join(root, file))
    return ret

EXTENSIONS = []

wrapper_root = "../src"
wrapper_source = pico_source = find_files(wrapper_root, '.c')
wrapper_source.remove('../src/yaml_get_key_component.c')
wrapper_source.remove('../src/read_xml.c')

wrapper_extention = Extension(
    "wrapper",["./module.pyx"]+wrapper_source,
    include_dirs=[],extra_compile_args=['-fopenmp'],
    extra_link_args=['-fopenmp']
)
if os.path.exists(wrapper_root):
    EXTENSIONS.append(wrapper_extention)



setup(
    packages=find_packages(),
    name="module",
    python_requires='>=3',
    ext_modules=cythonize(
        EXTENSIONS,
                compiler_directives={'language_level': 3, 'binding': False}
    ),

)