# distutils: language = c 

from libc.stdio cimport ( FILE)
from libc.stdint cimport int64_t
from libc.stdlib cimport malloc, free

cdef extern from *:
    """
    #define START_OMP_PARALLEL_PRAGMA() _Pragma("omp parallel") {
    #define END_OMP_PRAGMA() }
    #define START_OMP_SINGLE_PRAGMA() _Pragma("omp single") {
    #define START_OMP_CRITICAL_PRAGMA() _Pragma("omp critical") {   
    """
    void START_OMP_PARALLEL_PRAGMA() nogil
    void END_OMP_PRAGMA() nogil
    void START_OMP_SINGLE_PRAGMA() nogil
    void START_OMP_CRITICAL_PRAGMA() nogil

cdef extern from "zlib.h":
    ctypedef void *gzFile
    ctypedef int64_t z_off_t

    int gzclose(gzFile fp)
    gzFile gzopen(char *path, char *mode)

cdef extern from "../src/paths.h":
    struct path: 
        char **components
        int length
    
    struct node:
        const char *name
        const path *path
        char **values
        const int n_values
        const char **sub_tags
        const int n_sub_tags
        const char *attribute
        const char *expected_attribute
        FILE *out

    struct node_set:
        const char *root
        const int max_path_depth
        const int key_idx
        node **nodes
        const int n

    node_set *construct_node_set(char *structure_file, char *cache_dir,
                                int str_max)
    void release_node_set(node_set *ns)
    node_set *clone_node_set(node_set *ns, char *cache_dir, int thread,
                            int str_max)
    void release_clone(node_set *ns)

cdef extern from "../src/query.h":
    enum :
        EMPTY_TAG = 20,
        PREV_EMPTY_TAG,
        NO_ATTRIBUTE,
    
    int get_tag(gzFile fptr, char c, char s[], int str_max)
    int get_value(gzFile fptr, char c, char s[], int str_max)
    int get_attribute(gzFile fptr, char c, char s[], int str_max)

cdef extern from "../src/yaml_reader.h":
    enum:
        YAML__ERROR_FILE = 50,
        YAML__ERROR_KEY,
        YAML__ERROR_VALUE,
        YAML__WARN_BUFFER_OVERFLOW,

    int yaml_get_keys(const char *structure_file, char ***keys, int *n_keys,
                    const size_t str_max)

    int yaml_get_map_value(const char *structure_file, const char *key,
                        char *value, const size_t str_max)
                        
    int yaml_get_map_contents(const char *structure_file, const char *key,
                            char ***key_value_pairs, size_t *n_items)

cdef extern from "../src/read_xml.c":
    int STR_MAX
    int main(int argc, char** argv)


cdef build(argc, argv):
    cdef char** argV
    argV = <char**>malloc(argc * sizeof(char*))
    if argV is NULL:
            raise MemoryError()
    for i in range(argc):
            argv[i] = argv[i].encode()
            argV[i] = argv[i]
    return main(argc,argV)

