import requests
from base64 import b64encode

def get_publications(orcid):
    #currently I dont have client_id and secret
    client_id = 'YOUR_API_CLIENT_ID'
    client_secret = 'YOUR_API_CLIENT_SECRET'

  # Set the ORCID API endpoint and search parameters
    endpoint = f'https://pub.orcid.org/v3.0/{orcid}/works'
    params = {'sort': 'created-date', 'order': 'desc'}

    # Set the Authorization header with the API client credentials
    headers = {'Authorization': 'Basic ' + b64encode(f'{client_id}:{client_secret}'.encode()).decode()}

    # Send the GET request to the ORCID API endpoint with the search parameters and headers
    response = requests.get(endpoint, params=params, headers=headers)

    # Parse the JSON response and extract the publication titles
    publications = []
    if response.status_code == 200:
        data = response.json()
        for work in data['group']:
            if work['type'] == 'journal-article':
                title = work['work-summary'][0]['title']['title']['value']
                publications.append(title)
    else:
        print('Failed to retrieve publications:', response.status_code)

    return publications